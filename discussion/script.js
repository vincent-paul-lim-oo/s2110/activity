console.log(`Hello World`);

//JSON Objects
/* 
    JSON - JavaScript Object Notation
    JS Objects are not the same as JSON


    Syntax:
        {
            "propertyA": "valueA",
            "propertyB": "valueB"
        }
*/

//JSON Objects

/* {
    "city": "Quezon City",
    "province": "Metro Manila",
    "country": "Philippines"
} */

//JSON Array
/* "cities": [
    {
        "city": "Quezon City",
        "province": "Metro Manila",
        "country": "Philippines"
    }
    {
        "city": "Pasig City",
        "province": "Metro Manila",
        "country": "Philippines"
    }
    {
        "city": "Makati City",
        "province": "Metro Manila",
        "country": "Philippines"
    }
] */

//JSON METHOD
//The JSON Object contains methods for parsing and converting data into a stringified JSON

//Converting data into stringified JSON

let batchesArr = [
    {
        batchName: "Batch 157"
    },
    {
        batchName: "Batch 158"
    }
]

console.log(batchesArr);

//The stringify method is used to convert JS objects into a string
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
    name: `John`,
    age: 31,
    address: {
        city: `Manila`,
        country: `Philippines`,
    }
});

console.log(data)

/* 
    Syntax:
        JSON.stringify({
            propertyA: variableA,
            propertyB: variableB
        })
*/

//User details

/* let firstName = prompt(`What is your first name?`);
let lastName = prompt(`What is your last name`);
let age = prompt(`What is your age`);
let address = {
    city: prompt(`Which city do you live in?`),
    country: prompt(`Which country does your city address belong to`)
};

let otherData = JSON.stringify({
    firstName: firstName,
    lastName: lastName,
    age: age,
    address: address
})

console.log(otherData); */
//Before sending data, convert an array or an object to its string equivalent.
    //stringify()

let batchesJSON = `[
    {
        "batchName":"157"
    },
    {
        "batchName":"158"
    }    
]`

console.log(batchesJSON);
console.log(JSON.parse(batchesJSON));

//Upon receiving data, the JSON text can be converted to JS Object so that we can use it in our program
    //parse()

let stringifiedObject = `{
    "name": "John",
    "age": "31",
    "address": {
        "city": "Manila",
        "country": "Philippines"
    }
}`

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));